# Wireguard
I got the wireguard docker image with:
    
    sudo docker pull linuxserver/wireguard

I created a directory called wireguard in my user directory.
I then ran the container with:

    sudo docker run -d \
    --name=wireguard \
    --cap-add=NET_ADMIN \
    --cap-add=SYS_MODULE \
    -e PUID=1000 \
    -e PGID=1000 \
    -e TZ=America/Los_Angeles \
    -e SERVERURL=auto \
    -e PEERS=1 \
    -e PEERDNS=192.168.33.1 \
    -e INTERNAL_SUBNET=192.168.34.0/24 \
    -p 51820:51820/udp \
    -v /home/paulglobisch/wireguard:/config \
    --sysctl="net.ipv4.conf.all.src_valid_mark=1" \
    --restart unless-stopped \
    linuxserver/wireguard

For some reason the container failed to mount the config files. After some troubleshooting, I ran:

    sudo apt update
    sudo apt upgrade

Then, I removed the wireguard container from docker and re-ran it. This created the configuration files I needed. I took a look at peer1.conf and it looked good aside from the endpoint, which was a seemingly random ip address. I changed the endpoint to 192.168.64.13:51820. I also added the following to my pf.conf (in their appropriate sections):

    wg_net="192.168.34.0/24"

    nat on $ext_if from $wg_net to any -> ($ext_if)

    rdr pass on $ext_if proto udp from any to $ext_if port 51820 -> 192.168.33.9 port 51820

    pass out on $int_if proto udp from any to 192.168.33.9 port 51820

    pass in on $int_if from $wg_net to any
    pass out on $int_if from any to $wg_net

I then gave the peer1.conf file to my Mac through Samba. I downloaded the wire guard client on my Mac and tried connecting to the VPN with peer1.conf. It didn't work and there was no immediately apparent reason why. I checked the firewall log and there was nothing getting blocked. Whenever I tried to connect, my Mac and VMs would lose all internect access as well as some internal network connection. One of the first troubleshooting steps I came across involved looking at resolv.conf in Ubuntu. I checked and my resolv.conf symlink was broken and it didn't look like resolv.conf existed. Instead of trying to figure that out, I just created a new Ubuntu VM. This time it worked and the wireguard tunnel connected successfully. I double checked with netstat and my Mac was routing traffic through the VPN.

I added the following to my docker-compose.yml:

    wireguard:
        image: linuxserver/wireguard
        container_name: wireguard
        cap_add:
        - NET_ADMIN
        - SYS_MODULE
        environment:
        - PUID=1000
        - PGID=1000
        - TZ=America/Los_Angeles
        - SERVERURL=auto
        - PEERS=1
        - PEERDNS=192.168.33.1
        - INTERNAL_SUBNET=192.168.34.0/24
        ports:
        - "51820:51820/udp"
        volumes:
        - /home/paulglobisch/wireguard:/config
        sysctls:
        - net.ipv4.conf.all.src_valid_mark=1
        restart: unless-stopped

# Wazuh
I am on an ARM Mac and I took a look at https://jacobriggs.io/blog/posts/how-to-install-a-wazuh-siem-server-on-a-raspberry-pi-4b-26 for an idea of how to do it on an ARM system and decided otherwise. I made a new Ubuntu VM that emulated an x86 architecture. The new VM was notably slower, mostly during its setup and installation.

To install the Wazuh container I followed the steps in https://documentation.wazuh.com/current/deployment-options/docker/docker-installation.html and https://documentation.wazuh.com/current/deployment-options/docker/wazuh-container.html#exposed-ports extactly.

I also added the following to my pf.conf:

    rdr pass on $ext_if proto tcp from any to $ext_if port 443 -> 192.168.33.9 port 443

    pass out on $int_if proto tcp from any to 192.168.33.9 port 443

On FreeBSd, I ran:

    pkg install wazuh-agent 
It gave a message after running and I ran the cp command it told me to run. I then edited /var/ossec/etc/ossec.conf, replacing the IP in there with my Ubuntu IP address. There was also this line in there that I removed:

    <config-profile>debian, debian8</config-profile>
I read that there is supposed to be some authorizing or registering of an agent, but when I went to https://192.168.64.13 on a browser, Wazuh seemed to be working with the agent just fine.

![wazuh_screenshot](wazuh.png)
