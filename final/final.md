# GitLab
I downloaded a GitLab image (that's compatible with ARM) with:

    sudo docker pull zengxs/gitlab:latest
I then created the gitlab folder with data and log subfolder. I ran the container with:

    sudo docker run --detach \
    --hostname gitlab.container.com \
    --publish 8443:443 --publish 8080:80 --publish 22220:22 \
    --name gitlab \
    --restart always \
    --volume /home/paulglobisch/gitlab/data:/var/opt/gitlab \
    --volume /home/paulglobisch/gitlab/log:/var/log/gitlab \
    zengxs/gitlab:latest
I also added the following to my pf.conf:

    rdr pass on $ext_if proto tcp from any to $ext_if port 8443 -> 192.168.33.9 port 8443

    pass out on $int_if proto tcp from any to 192.168.33.9 port 8443

I added the following to my docker-compose.yml

    gitlab:
    image: 'zengxs/gitlab:latest'
    container_name: gitlab
    restart: always
    hostname: 'gitlab.container.com'
    ports:
      - '8443:443'
      - '8080:80'
      - '2222:22'
    volumes:
      - './gitlab/config:/etc/gitlab'
      - './gitlab/logs:/var/log/gitlab' 
      - './gitlab/data:/var/opt/gitlab' 
I could now access GitLab from my host machine.
![gitlab_screenshot](gitlab.png)

# BitWarden
I downloaded a BitWarden image with:

    sudo docker pull bitwardenrs/server
I then created the bitwarden folder with data and config subfolders. I ran the container with:

    docker run -d --name bitwarden \
    -e SIGNUPS_ALLOWED=false \
    -v ~/bitwarden/data:/data \
    -v ~/bitwarden/config:/config \
    -p 8888:80 \
    bitwardenrs/server
I also added the following to my pf.conf:

    rdr pass on $ext_if proto tcp from any to $ext_if port 8888 -> 192.168.33.9 port 8888

    pass out on $int_if proto tcp from any to 192.168.33.9 port 8888

I added the following to my docker-compose.yml

    bitwarden:
        image: bitwardenrs/server:latest
        container_name: bitwarden
        restart: always
        environment:
        - SIGNUPS_ALLOWED=false 
        volumes:
        - ./bitwarden/data:/data 
        - ./bitwarden/config:/config directory
        ports:
        - 8888:80
I could now access BitWarden from my host machine.
![bitwarden_screenshot](bitwarden.png)

# ZoneMinder
I could not find a functioning container for ZoneMinder that was ARM compatible and worked. I tried several, but the container never worked properly (it would "run" but would never actually create any files). I swapped to my x86 emulated Ubuntu VM. Many of them had the same issue. There was never any error message either. There seems to be a ton of deprecated images for ZoneMinder, so maybe there is some version issue.

I finally found and downloaded a working ZoneMinder image (credit to this [blog](https://bkjaya.wordpress.com/2021/05/15/how-to-install-zoneminder-1-36-0-focal1-on-ubuntu-20-04-lts-focal-fossa-using-a-docker-image/)) with:

    sudo docker pull bkjaya1952/docker-zoneminder-php7.4-mysql8:latest
I ran the container with:

    sudo docker run -d -t -p 8000:80 --name zm --privileged=true -e TZ=America/Los_Angeles bkjaya1952/docker-zoneminder-php7.4-mysql8:latest
I also added the following to my pf.conf:

    rdr pass on $ext_if proto tcp from any to $ext_if port 8000 -> 192.168.33.9 port 8000

    pass out on $int_if proto tcp from any to 192.168.33.9 port 8000
As 
I added the following to my docker-compose.yml

    zoneminder:
        image: bkjaya1952/docker-zoneminder-php7.4-mysql8:latest
        container_name: zm
        privileged: true
        environment:
        - TZ=America/Los_Angeles
        ports:
        - "8000:80"
        restart: unless-stopped
I could now access ZoneMinder from my host machine.
![zoneminder_screenshot](zoneminder.png)