# Samba
The first thing I did was install docker with:
    
    sudo apt install docker.io
I then got the samba image with:

    sudo docker pull dperson/samba
I made a directory called samba_folder for the container to use. I ran samba with:

    sudo docker run -it --name samba -p 139:139 -p 445:445 -v ~/samba_folder:/mount --restart='always' dperson/samba -s "public;/mount;yes;no;yes;all;none;public"

I added the following redirect rules to my pf.conf:

    rdr pass on $ext_if proto tcp from any to $ext_if port 139 -> 192.168.33.9 port 139
    rdr pass on $ext_if proto tcp from any to $ext_if port 445 -> 192.168.33.9 port 445
I can now connect to the samba server by connecting to smb://192.168.64.13/public on my Mac.

# Pi-hole
I downloaded the pi-hole image with:

    sudo docker pull pihole/pihole
and added a network for pi-hole with:

    sudo docker network create --driver bridge pihole_network
I then tried running pi-hole with:

    sudo docker run -d --name pihole -p 53:53/tcp -p 53:53/udp -p 67:67/udp -p 80:80 -p 443:443 -e TZ="America/Los_Angeles" -v "$(pwd)/etc-pihole/:/etc/pihole/" -v "$(pwd)/etc-dnsmasq.d/:/etc/dnsmasq.d/" --dns=127.0.0.1 --dns=1.1.1.1 -restart=unless-stopped --network=pihole_network --hostname pi.hole -e VIRTUAL_HOST="pi.hole" -e PROXY_LOCATION="pi.hole" -e ServerIP="192.168.33.9" pihole/pihole:latest

It failed because socket 53 was already in use by systemd-resolved. Since pi-hole functions as a DNS server, I figured I could just disable systemd-resolved with:

    sudo systemctl stop systemd-resolved
    sudo systemctl disable systemd-resolved
After that, I had to change the "nameserver" in resolv.conf from "127.0.0.53" to "127.0.0.1" so Ubuntu would use pi-hole for DNS correctly.

To connect to pi-hole I added the following to my Mac's ssh config, so port 80 on Ubuntu gets forwarded to 8080 on my Mac when the ssh connection is active:

    Host pihole
        HostName 192.168.64.13
        Port 22
        LocalForward 8080 localhost:80

After that, I could access pi-hole with http://localhost:8080/admin/ on a web browser.

# Docker Compose
I installed docker-compose with:

    sudo apt install docker-compose
I created the following docker-compose.yml:

    version: '3'

    services:
    samba:
        image: dperson/samba
        restart: unless-stopped
        ports:
        - "139:139"
        - "445:445"
        volumes:
        - ./shared-folder:/mount
        command: '-s "public;/mount;yes;no;yes;all;none;public"'

    pihole:
        image: pihole/pihole:latest
        container_name: pihole
        network_mode: bridge
        restart: unless-stopped
        ports:
        - "53:53/tcp"
        - "53:53/udp"
        - "67:67/udp"
        - "80:80"
        - "443:443"
        environment:
        TZ: "America/Los_Angeles"
        volumes:
        - ./etc-pihole/:/etc/pihole/
        - ./etc-dnsmasq.d/:/etc/dnsmasq.d/

    networks:
    default:
        external:
        name: pihole_network

I had some issues with permissions when trying to run it, so I added my user to the docker group with:

    sudo usermod -aG docker $USER

