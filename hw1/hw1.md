# Screenshots
![freebsd_output](freebsd_ifconfig.png)

![ubuntu_output](ubuntu_ip_a_s.png)

# VM Installation
I am using an M1 Mac for this class.

In the process of installing the FreeBSD VM and the Ubuntu VM, I ran into an issue; UTM was not ejecting the ISO file after installation for whatever reason. On Ubuntu, I was able to eject the ISO after the installer rebooted, and then restarted the VM with UTM. On FreeBSD, I had to go into the shell using the option provided at the end of installation (before any reboots), poweroff from there, and then clear the ISO in the UTM menu. Other than that, the installation of the VMs went smoothly.

# Ubuntu Configuration

I had a problem with Ubuntu's IP address. Ubuntu was getting its IP address from the Bridge Interface created for it by UTM (192.168.128.1/24), instead of getting it from FreeBSD. I checked the arp table on FreeBSD and did not see Ubuntu on it. I tried various methods of restarting network services on both FreeBSD and Ubuntu. I also double checked dnsmasq.conf and even reinstalled both VMs. 

Nothing worked, so I installed a fresh Ubuntu VM and statically set the IP to 192.168.33.9/24 in the Network Configuration step. After that, Ubuntu was able to communicate with FreeBSD and I could also ssh into the Ubuntu VM with FreeBSD as a bastion host. I double checked that Ubuntu was actually routing through FreeBSD through tcpdump; when I pinged google.com, I could see that FreeBSD had packets coming through on vtnet1 with Ubuntu as the origin and google.com's IP as the destiantion.

The rest of the Ubuntu configuration went by without any issues.

# ~/.ssh/config Contents
The following is the contents of my ~/.ssh/config file:
```
Host ubuntu
    Hostname 192.168.33.9
    ProxyJump 192.168.64.13

Host freebsd
    Hostname 192.168.64.13
```
