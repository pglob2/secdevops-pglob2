# Adding a redirect rule
This is my pf.conf after adding a redirect rule:

    rdr pass on $ext_if proto tcp from any to $ext_if port 22 -> 192.168.33.9 port 22

I tried sshing using the new redirect, but it did not work. I checked pflog and I saw that the packets were getting blocked out to vtnet1. So I added the following pass rule: 

    pass out on $int_if proto tcp from any to 192.168.33.9 port 22
This did the trick and I could ssh to ubuntu directly now.

# Modifying ssh server
I modified ssh_config and changed "#Port 22" to "Port 2222". I also did the same to sshd_config. I also added
a modified version of the

    pass in on $ext_if proto tcp to port { ssh } keep state (max-src-conn 15, max-src-conn-rate 3/1, overload <bruteforce> flush global)
rule, changing "{ ssh }" to "{ 2222 }".


# Full pf.conf

    ext_if="vtnet0"
    int_if="vtnet1"

    icmp_types = "{ echoreq, unreach }"
    services = "{ ssh, domain, http, ntp, https }"
    server = "192.168.33.63"
    ssh_rdr = "2222"
    table <rfc6890> { 0.0.0.0/8 10.0.0.0/8 100.64.0.0/10 127.0.0.0/8 169.254.0.0/16          \
                    172.16.0.0/12 192.0.0.0/24 192.0.0.0/29 192.0.2.0/24 192.88.99.0/24    \
                    192.168.0.0/16 198.18.0.0/15 198.51.100.0/24 203.0.113.0/24            \
                    240.0.0.0/4 255.255.255.255/32 }
    table <bruteforce> persist


    #options
    set skip on lo0

    #normalization
    scrub in all fragment reassemble max-mss 1440

    #NAT rules
    nat on $ext_if from $int_if:network to any -> ($ext_if)

    #redirect rules
    rdr pass on $ext_if proto tcp from any to $ext_if port 22 -> 192.168.33.9 port 22

    #blocking rules
    antispoof quick for $ext_if
    block in quick on egress from <rfc6890>
    block return out quick on egress to <rfc6890>
    block log all

    #pass rules
    pass in quick on $int_if inet proto udp from any port = bootpc to 255.255.255.255 port = bootps keep state label "allow access to DHCP server"
    pass in quick on $int_if inet proto udp from any port = bootpc to $int_if:network port = bootps keep state label "allow access to DHCP server"
    pass out quick on $int_if inet proto udp from $int_if:0 port = bootps to any port = bootpc keep state label "allow access to DHCP server"

    pass in quick on $ext_if inet proto udp from any port = bootps to $ext_if:0 port = bootpc keep state label "allow access to DHCP client"
    pass out quick on $ext_if inet proto udp from $ext_if:0 port = bootpc to any port = bootps keep state label "allow access to DHCP client"

    pass in on $ext_if proto tcp to port { 2222 } keep state (max-src-conn 15, max-src-conn-rate 3/1, overload <bruteforce> flush global)
    pass in on $ext_if proto tcp to port { ssh } keep state (max-src-conn 15, max-src-conn-rate 3/1, overload <bruteforce> flush global)
    pass out on $ext_if proto { tcp, udp } to port $services
    pass out on $ext_if inet proto icmp icmp-type $icmp_types
    pass in on $int_if from $int_if:network to any
    pass out on $int_if from $int_if:network to any
    pass out on $int_if proto tcp from any to 192.168.33.9 port 22

# Installing Snort
I installed snort with 

    pkg install snort 
and then inside the rules folder, I got the community ruleset with

    fetch https://www.snort.org/downloads/community/community-rules.tar.gz
and then expanded it and moved it with

    tar -xvzf community-rules.tar.gz -C .
    mv /community-rules/community.rules .

In Snort.conf, I added a line for the community rules

    include $RULE_PATH/community.rules
And also deleted all the other rule includes aside from the local.rules one. I created an empty "local.rules" file in the rules folder. When trying to run Snort with

    snort -v -c /usr/local/etc/snort/snort.conf
I got the following error

    ERROR: /usr/local/etc/snort/snort.conf(512) => Unable to open address file /usr/local/etc/snort/../rules/white_list.rules, Error: No such file or directory

This was the cause of many hours debugging and reinstalling Snort. For whatever reason, I had commented out the part regarding white_list.rules in snort.conf in an attempt to get the error to go away. I'm not sure why, but this really messed up Snort and it led to a chain of errors that never ended. After a while, I read that I need to just create an empty white_list.rules and black_list.rules file. I also had to change the path for WHITE_LIST_PATH and BLACK_LIST_PATH because they were "../rules" instead of "./rules".

After all that, running Snort didn't give any errors.

# Snort as a service
The next thing I did was add
    snort_enable="YES"
to rc.conf. After a reboot, 
    service snort status
showed that Snort was still running.

# Testing Snort
I didn't do a ton of testing on Snort. I checked the alert log and found that the ssh connection I was using set off a couple alerts, namely "TCP session without 3-way handshake" and "Consecutive TCP small segments exceeding threshold". I took this as indication that it was functioning.

# SMBGhost
I found two Snort rules for SMBGhost at https://gist.github.com/austinsonger/b39063152efe2fa68d0b56226b797348
    block tcp any any -> any 445 (msg:"Claroty Signature: SMBv3 Used with compression - Client to server"; content:"|fc 53 4d 42|"; offset: 0; depth: 10; sid:1000001; rev:1; reference:url,//blog.claroty.com/advisory-new-wormable-vulnerability-in-microsoft-smbv3;)

    block tcp any 445 -> any any (msg:"Claroty Signature: SMBv3 Used with compression - Server to client"; content:"|fc 53 4d 42|"; offset: 0; depth: 10; sid:1000002; rev:1; reference:url,//blog.claroty.com/advisory-new-wormable-vulnerability-in-microsoft-smbv3;)
The rules are not very specific and just block packets with the SMB header (53 4d 42 is "SMB") (https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-cifs/69a29f73-de0c-45a6-a1aa-8ceeea42217f) and the compression flag. I tried testing it with the POC, but couldn't wrap my head around how to get the packets I want out of it. I tried running the python script, but couldn't manage to get more than TCP handshake packets out of it. This was definitely too much to leave for 1 day.
