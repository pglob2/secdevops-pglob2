#!/bin/sh
#
# To download this script directly from freeBSD:
# $ pkg install curl
# $ curl -LO https://gitlab.cecs.pdx.edu/pglob2/secdevops-pglob2/-/raw/main/hw2/hw2.sh
#

WAN="vtnet0"
LAN="vtnet1"

cd ~/../..

# Install dnsmasq
pkg install -y dnsmasq

# Enable forwarding
sysrc gateway_enable="YES"
# Enable immediately
sysctl net.inet.ip.forwarding=1

# Set LAN IP
ifconfig ${LAN} inet 192.168.33.1 netmask 255.255.255.0
# Make IP setting persistent
sysrc "ifconfig_${LAN}=inet 192.168.33.1 netmask 255.255.255.0"

ifconfig ${LAN} up
ifconfig ${LAN} promisc

# Enable dnsmasq on boot
sysrc dnsmasq_enable="YES"

# Edit dnsmasq configuration
if ! grep -q "interface=${LAN}" /usr/local/etc/dnsmasq.conf; then
    echo "interface=${LAN}" >> /usr/local/etc/dnsmasq.conf
    echo "dhcp-range=192.168.33.50,192.168.33.150,12h" >> /usr/local/etc/dnsmasq.conf
    echo "dhcp-option=option:router,192.168.33.1" >> /usr/local/etc/dnsmasq.conf
fi

# Download pf.conf
cd /etc
if [ -f "/etc/pf.conf" ]; then
    rm pf.conf
fi
curl -LO https://gitlab.cecs.pdx.edu/pglob2/secdevops-pglob2/-/raw/main/hw2/pf.conf

# Start dnsmasq
service dnsmasq start

# Enable PF on boot
sysrc pf_enable="YES"
sysrc pflog_enable="YES"

# Start PF
service pf start

# Load PF rules
pfctl -f /etc/pf.conf

# Modify ssh port
sed -i '' 's/^#   Port 22/    Port 2222/' /etc/ssh/ssh_config
sed -i '' 's/^#Port 22/Port 2222/' /etc/ssh/sshd_config

# Install Snort
pkg install -y snort
cd ~/../..
cd usr/local/etc/snort/rules

# Download community rules
if [ ! -f "./community.rules" ]; then
    fetch https://www.snort.org/downloads/community/community-rules.tar.gz
    tar -xvzf community-rules.tar.gz -C .
    mv ./community-rules/community.rules .
fi

# Download snort.conf
cd ..
if [ -f "./snort.conf" ]; then
    rm snort.conf
fi
curl -LO https://gitlab.cecs.pdx.edu/pglob2/secdevops-pglob2/-/raw/main/hw2/snort.conf

# Add files required by snort.conf
touch ./rules/local.rules
touch ./rules/white_list.rules
touch ./rules/black_list.rules

# Add SMBGhost rule
echo "
block tcp any any -> any 445 (msg:\"Claroty Signature: SMBv3 Used with compression - Client to server\"; content:\"|fc 53 4d 42|\"; offset: 0; depth: 10; sid:1000001; rev:1; reference:url,//blog.claroty.com/advisory-new-wormable-vulnerability-in-microsoft-smbv3;)
block tcp any 445 -> any any (msg:\"Claroty Signature: SMBv3 Used with compression - Server to client\"; content:\"|fc 53 4d 42|\"; offset: 0; depth: 10; sid:1000002; rev:1; reference:url,//blog.claroty.com/advisory-new-wormable-vulnerability-in-microsoft-smbv3;)
" > ./rules/local.rules

# Enable Snort as a service
sysrc snort_enable="YES"
